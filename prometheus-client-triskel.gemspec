# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'prometheus/client/triskel/version'

Gem::Specification.new do |spec|
  spec.name          = 'prometheus-client-triskel'
  spec.version       = Prometheus::Client::Triskel::VERSION
  spec.authors       = ['Trileuco Solutions, S.L.U.']
  spec.email         = ['info@trileucosolutions.com']

  spec.summary       = 'Rack middleware to trace HTTP requests to a Rails application to be queried by Prometheus.'
  spec.description   = 'This gem includes a custom implementation of a Rack middleware to trace HTTP requests to a
                          Rails application to be queried by Prometheus thanks to the prometheus-client gem.'
  spec.homepage      = 'https://bitbucket.org/trileuco/prometheus-client-triskel'
  spec.license       = 'MIT'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.required_ruby_version = '~> 2.5'
  spec.add_runtime_dependency 'actionpack', '>= 4', '< 7'
  spec.add_runtime_dependency 'prometheus-client', '~> 2.1'
  spec.add_development_dependency 'appraisal', '~> 2.3'
  spec.add_development_dependency 'bundler', '~> 1.17'
  spec.add_development_dependency 'guard-rspec', '~> 4.7'
  spec.add_development_dependency 'rack-test', '~> 0.6'
  spec.add_development_dependency 'railties', '>= 4', '< 7'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'simplecov', '~> 0.18'
end
