# frozen_string_literal: true

require 'active_support'
require 'action_dispatch'
require 'rack/test'
require 'rails/engine'

require 'prometheus/client/triskel/middleware/collector'

RSpec.describe Prometheus::Client::Triskel::Middleware::Collector do
  include Rack::Test::Methods

  # Reset the data store
  before do
    Prometheus::Client.config.data_store = Prometheus::Client::DataStores::Synchronized.new
    app
  end

  let(:registry) { Prometheus::Client::Registry.new }
  let(:original_app) { ->(_) { [200, { 'Content-Type' => 'text/html' }, ['OK']] } }
  let(:app) { described_class.new(original_app, registry: registry) }

  let(:dummy_error) { RuntimeError.new('Dummy error from tests') }

  it 'returns the app response', :aggregate_failures do
    get '/foo'

    expect(last_response).to be_ok
    expect(last_response.body).to eql('OK')
  end

  it 'handles errors in the registry gracefully', :aggregate_failures do
    counter = registry.get(:http_server_requests_total)
    allow(counter).to receive(:increment).and_raise(dummy_error)

    get '/foo'

    expect(last_response).to be_ok
  end

  # rubocop:disable RSpec/ExampleLength
  it 'traces request information', :aggregate_failures do
    allow(Benchmark).to receive(:realtime).and_yield.and_return(0.2)

    get '/foo'

    metric = :http_server_requests_total
    labels = { method: 'get', path: '/foo', code: '200' }
    expect(registry.get(metric).get(labels: labels)).to be(1.0)

    metric = :http_server_request_duration_seconds
    labels = { method: 'get', path: '/foo' }
    expect(registry.get(metric).get(labels: labels)).to include('count' => 1, 'sum' => 0.2)
    expect(registry.get(metric).get(labels: labels)).not_to include('0.1', '0.25')
  end
  # rubocop:enable RSpec/ExampleLength

  # rubocop:disable RSpec/ExampleLength
  it 'includes SCRIPT_NAME in the path if provided', :aggregate_failures do
    metric = :http_server_requests_total

    get '/foo'
    expect(registry.get(metric).values.keys.last[:path]).to eql('/foo')

    env('SCRIPT_NAME', '/engine')
    get '/foo'
    env('SCRIPT_NAME', nil)
    expect(registry.get(metric).values.keys.last[:path]).to eql('/engine/foo')
  end
  # rubocop:enable RSpec/ExampleLength

  # rubocop:disable RSpec/ExampleLength
  it 'normalizes paths containing numeric IDs by default', :aggregate_failures do
    allow(Benchmark).to receive(:realtime).and_yield.and_return(0.3)

    get '/foo/42/bars'

    metric = :http_server_requests_total
    labels = { method: 'get', path: '/foo/:id/bars', code: '200' }
    expect(registry.get(metric).get(labels: labels)).to be(1.0)

    metric = :http_server_request_duration_seconds
    labels = { method: 'get', path: '/foo/:id/bars' }
    expect(registry.get(metric).get(labels: labels)).to include('count' => 1, 'sum' => 0.3)
  end
  # rubocop:enable RSpec/ExampleLength

  # rubocop:disable RSpec/ExampleLength
  it 'normalizes paths containing UUIDs by default', :aggregate_failures do
    allow(Benchmark).to receive(:realtime).and_yield.and_return(0.3)

    get '/foo/5180349d-a491-4d73-af30-4194a46bdff3/bars'

    metric = :http_server_requests_total
    labels = { method: 'get', path: '/foo/:uuid/bars', code: '200' }
    expect(registry.get(metric).get(labels: labels)).to be(1.0)

    metric = :http_server_request_duration_seconds
    labels = { method: 'get', path: '/foo/:uuid/bars' }
    expect(registry.get(metric).get(labels: labels)).to include('count' => 1, 'sum' => 0.3)
  end
  # rubocop:enable RSpec/ExampleLength

  context 'when the app raises an exception' do
    let(:original_app) do
      lambda do |env|
        raise dummy_error if env['PATH_INFO'] == '/broken'

        [200, { 'Content-Type' => 'text/html' }, ['OK']]
      end
    end

    before do
      get '/foo'
    end

    it 'traces exceptions', :aggregate_failures do
      expect { get '/broken' }.to raise_error RuntimeError

      metric = :http_server_exceptions_total
      labels = { exception: 'RuntimeError' }
      expect(registry.get(metric).get(labels: labels)).to be(1.0)
    end
  end

  context 'when provided a custom metrics_prefix' do
    let(:app) { described_class.new(original_app, registry: registry, metrics_prefix: 'lolrus') }

    it 'provides alternate metric names', :aggregate_failures do
      expect(registry.get(:lolrus_requests_total)).to be_a(Prometheus::Client::Counter)
      expect(registry.get(:lolrus_request_duration_seconds)).to be_a(Prometheus::Client::Summary)
      expect(registry.get(:lolrus_exceptions_total)).to be_a(Prometheus::Client::Counter)
    end

    it "doesn't register the default metrics", :aggregate_failures do
      expect(registry.get(:http_server_requests_total)).to be(nil)
      expect(registry.get(:http_server_request_duration_seconds)).to be(nil)
      expect(registry.get(:http_server_exceptions_total)).to be(nil)
    end
  end

  context 'when configured to use histograms' do
    let(:app) { described_class.new(original_app, registry: registry, use_histograms_for_durations: true) }

    it 'traces request durations as a histogram', :aggregate_failures do
      allow(Benchmark).to receive(:realtime).and_yield.and_return(0.2)

      get '/foo'

      metric = :http_server_request_duration_seconds
      labels = { method: 'get', path: '/foo' }
      expect(registry.get(metric).get(labels: labels)).to include('0.1' => 0, '0.25' => 1)
    end

    it 'provides the durations as a histogram metric', :aggregate_failures do
      expect(registry.get(:http_server_request_duration_seconds)).to be_a(Prometheus::Client::Histogram)
    end
  end

  context 'when provided a route set' do
    before do
      route_set.draw do
        get '/foo/:foo_bar_id/bar', to: ->(_) { [200, { 'Content-Type' => 'text/html' }, ['OK']] }
      end
    end

    let(:app) { described_class.new(original_app, registry: registry, app_routes: route_set) }
    let(:route_set) { ActionDispatch::Routing::RouteSet.new }

    # rubocop:disable RSpec/ExampleLength
    it 'is used to normalize the paths', :aggregate_failures do
      allow(Benchmark).to receive(:realtime).and_yield.and_return(0.3)

      get '/foo/friendly_slug/bar'

      metric = :http_server_requests_total
      labels = { method: 'get', path: '/foo/:foo_bar_id/bar', code: '200' }
      expect(registry.get(metric).get(labels: labels)).to be(1.0)

      metric = :http_server_request_duration_seconds
      labels = { method: 'get', path: '/foo/:foo_bar_id/bar' }
      expect(registry.get(metric).get(labels: labels)).to include('count' => 1, 'sum' => 0.3)
    end
    # rubocop:enable RSpec/ExampleLength

    # rubocop:disable RSpec/ExampleLength
    it 'doesn\'t normalize the path if it is not recognized by the routes', :aggregate_failures do
      allow(Benchmark).to receive(:realtime).and_yield.and_return(0.3)

      get '/bar/friendly_slug/foo'

      metric = :http_server_requests_total
      labels = { method: 'get', path: '/bar/friendly_slug/foo', code: '200' }
      expect(registry.get(metric).get(labels: labels)).to be(1.0)

      metric = :http_server_request_duration_seconds
      labels = { method: 'get', path: '/bar/friendly_slug/foo' }
      expect(registry.get(metric).get(labels: labels)).to include('count' => 1, 'sum' => 0.3)
    end
    # rubocop:enable RSpec/ExampleLength

    context 'when configure a param to not be normalized' do
      let(:app) do
        described_class.new(original_app, registry: registry, app_routes: route_set,
                                          routes_params_to_not_normalize: %w[format foo_bar_id])
      end

      # rubocop:disable RSpec/ExampleLength
      it 'doesn\'t normalize the parameter in the paths', :aggregate_failures do
        allow(Benchmark).to receive(:realtime).and_yield.and_return(0.3)

        get '/foo/friendly_slug/bar'

        metric = :http_server_requests_total
        labels = { method: 'get', path: '/foo/friendly_slug/bar', code: '200' }
        expect(registry.get(metric).get(labels: labels)).to be(1.0)

        metric = :http_server_request_duration_seconds
        labels = { method: 'get', path: '/foo/friendly_slug/bar' }
        expect(registry.get(metric).get(labels: labels)).to include('count' => 1, 'sum' => 0.3)
      end
      # rubocop:enable RSpec/ExampleLength
    end

    context 'when a engine is mounted in the routes' do
      before do
        stub_const('TestEngine::Engine', Class.new(::Rails::Engine))

        TestEngine::Engine.routes.draw do
          get '/engine_foo/:engine_id/engine_bar', to: ->(_) { [200, { 'Content-Type' => 'text/html' }, ['OK ENGINE']] }
        end

        route_set.draw do
          mount TestEngine::Engine, at: '/status'
          get '/foo/:foo_bar_id/bar', to: ->(_) { [200, { 'Content-Type' => 'text/html' }, ['OK']] }
        end
      end

      # rubocop:disable RSpec/ExampleLength
      it 'uses the engine routes to normalize the paths', :aggregate_failures do
        allow(Benchmark).to receive(:realtime).and_yield.and_return(0.3)

        env('SCRIPT_NAME', '/status')
        get '/engine_foo/friendly_slug/engine_bar'
        env('SCRIPT_NAME', nil)

        metric = :http_server_requests_total
        labels = { method: 'get', path: '/status/engine_foo/:engine_id/engine_bar', code: '200' }
        expect(registry.get(metric).get(labels: labels)).to be(1.0)

        metric = :http_server_request_duration_seconds
        labels = { method: 'get', path: '/status/engine_foo/:engine_id/engine_bar' }
        expect(registry.get(metric).get(labels: labels)).to include('count' => 1, 'sum' => 0.3)
      end
      # rubocop:enable RSpec/ExampleLength

      # rubocop:disable RSpec/ExampleLength
      it 'still normalizes the application paths', :aggregate_failures do
        allow(Benchmark).to receive(:realtime).and_yield.and_return(0.3)

        get '/foo/friendly_slug/bar'

        metric = :http_server_requests_total
        labels = { method: 'get', path: '/foo/:foo_bar_id/bar', code: '200' }
        expect(registry.get(metric).get(labels: labels)).to be(1.0)

        metric = :http_server_request_duration_seconds
        labels = { method: 'get', path: '/foo/:foo_bar_id/bar' }
        expect(registry.get(metric).get(labels: labels)).to include('count' => 1, 'sum' => 0.3)
      end
      # rubocop:enable RSpec/ExampleLength
    end

    context 'when configure paths to not be normalized' do
      let(:app) do
        described_class.new(original_app, registry: registry, app_routes: route_set,
                                          paths_to_not_normalize: ['/foo/friendly_slug/bar'])
      end

      # rubocop:disable RSpec/ExampleLength
      it 'doesn\'t normalize the path', :aggregate_failures do
        allow(Benchmark).to receive(:realtime).and_yield.and_return(0.3)

        get '/foo/friendly_slug/bar'

        metric = :http_server_requests_total
        labels = { method: 'get', path: '/foo/friendly_slug/bar', code: '200' }
        expect(registry.get(metric).get(labels: labels)).to be(1.0)

        metric = :http_server_request_duration_seconds
        labels = { method: 'get', path: '/foo/friendly_slug/bar' }
        expect(registry.get(metric).get(labels: labels)).to include('count' => 1, 'sum' => 0.3)
      end
      # rubocop:enable RSpec/ExampleLength
    end
  end
end
