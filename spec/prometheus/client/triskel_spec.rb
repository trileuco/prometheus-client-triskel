# frozen_string_literal: true

require 'prometheus/client/triskel'

RSpec.describe Prometheus::Client::Triskel do
  it 'has a version number' do
    expect(Prometheus::Client::Triskel::VERSION).not_to be nil
  end
end
