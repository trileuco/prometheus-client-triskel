# Prometheus::Client::Triskel

This project provides a gem with a custom implementation of a Rack middleware to trace HTTP requests to a Rails application to be queried by Prometheus thanks to the prometheus-client gem.

## Installation

Add this line to your application's Gemfile (adapting the tag to the one you wish to use):

```ruby
gem 'prometheus-client-triskel', git: 'https://bitbucket.org/trileuco/prometheus-client-triskel.git', tag: 'v1.0.0'
```

And then execute:

    $ gem install bundler -v 1.17
    $ bundle

Or install it yourself as:

    $ gem install prometheus-client-triskel

## Usage

To configure the gem in your rails application you must follow the instructions defined on the ruby `prometheus-client` gem README on: https://github.com/prometheus/client_ruby/tree/v2.1.0#rack-middleware where they explain how to configure the `Collector` and `Exporter` middlewares. To configre the custom `Collector` middleware provided by this gem you only have to configure it like this:

```ruby
use Prometheus::Client::Triskel::Middleware::Collector
```

You have currently the following options that you can use with this middleware:

* `use_histograms_for_durations`: this allow us configure the collector, to use `Summary` metrics or `Histogram` metrics for the request durations. By default the option will have a `false` value, so `Summary` metrics will be used.

* `app_routes`: in the rails applications we can pass here the application routes with `Rails.application.routes` and this will allow the collector to use this rails routes to normalize the request paths and avoid to create a very big number of metrics. By default the it will have a `nil` value and we will use the default `strip_ids_from_path` method defined on the original collector.

* `routes_params_to_not_normalize`: this option only will be used when an `app_routes` param be defined, and it let us define a list of params on the routes that won't be normalized. The default value will be: `['format']`

* `paths_to_not_normalize`: with with configuration option we can add a list of paths that won't be tried to be normalized. By default it will use the value: `['/metrics']`

A tipical configuration of the collector could be:

```ruby
use Prometheus::Client::Triskel::Middleware::Collector, app_routes: Rails.application.routes
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To test the code with different combinations of gem versions (in this case with Rails 4, Rails 5 and Rails 6) we use the appraisals gem. To configure it you can use the following command, this will install the gems needed for each Gemfile of the project based on the Appraisals file:

```bash
bundle exec appraisal install
```

Finally we can execute the tests with each combination of gems using the following command (that will also will be the default executing just `rake`):

```bash
bundle exec appraisal rspec
```

## Code syntax and conventions checks

We use overcommit for automated git hooks that validate code syntax and conventions. To configure overcommit go to the project root and execute:

```bash
bundle install --gemfile=.overcommit_gems.rb
overcommit --install
```

Check that everything is working correctly with:

```bash
overcommit -r
```

Now before each commit the validations will be executed and if something isn't correct the commit won't be done.

## Contributing

Bug reports and pull requests are welcome on Bitbucket at https://bitbucket.org/trileuco/prometheus-client-triskel. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Prometheus::Client::Triskel project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://bitbucket.org/trileuco/prometheus-client-triskel/src/master/CODE_OF_CONDUCT.md).
