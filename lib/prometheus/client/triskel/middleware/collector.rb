# frozen_string_literal: true

require 'active_support/core_ext/hash/indifferent_access'
require 'active_support/core_ext/object/try'
require 'prometheus/middleware/collector'

module Prometheus
  module Client
    module Triskel
      module Middleware
        class Collector < Prometheus::Middleware::Collector
          def initialize(app, options = {})
            options = default_opts.merge(options)
            @use_histograms_for_durations = options[:use_histograms_for_durations]
            @app_routes = options[:app_routes]
            @routes_params_to_not_normalize = options[:routes_params_to_not_normalize]
            @paths_to_not_normalize = options[:paths_to_not_normalize]
            super(app, options)
          end

          protected

          def default_opts
            { use_histograms_for_durations: false,
              app_routes: nil,
              routes_params_to_not_normalize: ['format'],
              paths_to_not_normalize: ['/metrics'] }
          end

          def init_request_metrics
            @requests = init_request_total_metrics
            @durations = init_duration_metrics
          end

          def record(env, code, duration)
            method = env['REQUEST_METHOD'].downcase
            path = [env['SCRIPT_NAME'], env['PATH_INFO']].join
            path = normalize_path(env, path)

            @requests.increment(labels: { code: code, method: method, path: path })
            @durations.observe(duration, labels: { method: method, path: path })
          rescue StandardError => e
            @exceptions.increment(labels: { exception: e.class.name })
          end

          def normalize_path(env, path)
            return path if @paths_to_not_normalize.include?(path)

            @app_routes.present? ? normalize_path_with_routes(env, path) : strip_ids_from_path(path)
          end

          def normalize_path_with_routes(env, path)
            script_name = env['SCRIPT_NAME']
            routes = routes_for_engine(script_name)
            return path if routes.nil? || routes.try(:router).nil?

            request = Rack::Request.new(env)
            routes.router.recognize(request) do |route, params|
              return "#{script_name}#{route.format(normalize_path_params(route.path.names, params))}"
            end

            path
          end

          private

          def init_request_total_metrics
            @registry.counter(
              :"#{@metrics_prefix}_requests_total",
              docstring: 'The total number of HTTP requests handled by the Rack application.',
              labels: %i[code method path]
            )
          end

          def init_duration_metrics
            durations_metric = :"#{@metrics_prefix}_request_duration_seconds"
            durations_docstring = 'The HTTP response duration of the Rack application.'
            durations_labels = %i[method path]
            duration_options = { docstring: durations_docstring, labels: durations_labels }

            if @use_histograms_for_durations
              @registry.histogram(durations_metric, duration_options)
            else
              @registry.summary(durations_metric, duration_options)
            end
          end

          def routes_for_engine(script_name)
            return @app_routes unless script_name.present?

            engine_route = @app_routes.routes.routes.detect { |r| r.path.match(script_name) }
            engine_route&.app&.app.try(:routes)
          end

          def normalize_path_params(route_path_names, params)
            route_path_names_map = route_path_names.map { |n| [n, ":#{n}"] }.to_h
            params.with_indifferent_access.merge(route_path_names_map.except(*@routes_params_to_not_normalize))
          end
        end
      end
    end
  end
end
