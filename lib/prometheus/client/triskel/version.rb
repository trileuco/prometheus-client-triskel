# frozen_string_literal: true

# :nocov:
module Prometheus
  module Client
    module Triskel
      VERSION = '1.0.0'
    end
  end
end
# :nocov:
