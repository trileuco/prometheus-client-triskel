# Contributing

This repository is following the *Triskel Services Contributing* guidelines.

View the documentation [here](https://bitbucket.org/trileuco/triskel-services-docs/src/master/CONTRIBUTING.md) to find the last information.
