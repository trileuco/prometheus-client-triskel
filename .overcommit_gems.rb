# frozen_string_literal: true

source 'https://rubygems.org'

gem 'overcommit', '~> 0.57.0'
gem 'rubocop', '~> 0.93.1'
gem 'rubocop-performance', '~> 1.8.1'
gem 'rubocop-rspec', '~> 1.43.2'
